// Problem 2 - https://adventofcode.com/2018/day/2

// Part 1
// This problem asks us to take our sample input and count the number of lines that
// contain exactly two or exactly three repeated letters. Then, we take the "checksum"
// by multiplying those two numbers together.

// Part 2
// The part of the problem asks us to find, out of the list of IDs that met the criteria
// in part one, two IDs who vary by only one character where the characters are both
// located in the same place in each string.


// Something really cool about Rust is that most short programs that successfully
// compile normally don't need any debugging (or very little). For part two of this
// problem, I ran the program one time only. I don't think I could have done that in any
// other language.


// Importing the local advent library
extern crate adventlib;
use adventlib::return_file_as_str_vector;

fn main() {
    let input_vector = return_file_as_str_vector("input.txt");
    let (part_one_result, part_two_vector) = part_one(input_vector);
    let part_two_result = part_two(part_two_vector);

    println!("The answer to part one is [{}]", part_one_result);
    println!("The answer to part one is [{}]", part_two_result);
}

fn part_one(input_vector: Vec<String>) -> (i32, Vec<String>) {
    let mut twos_count = 0;
    let mut threes_count = 0;

    let mut id_list: Vec<String> = [].to_vec();

    for line in input_vector {
        let (two, three) = count_letters(&line);

        // If either two or three are non-zero, then add the ID to the list of potential
        // IDs that part two requires
        if (two != 0) | (three != 0) {
            id_list.push(line.to_string());
        }

        // Add the two parts of the tuple to our counts
        twos_count += two;
        threes_count += three;
    }

    let checksum = twos_count * threes_count;
    return (checksum, id_list);
}

fn count_letters(word: &String) -> (i32, i32) {
    // The idea behind this function is to create a vector with 26 members corresponding
    // to each letter. Then, we use another function with a match statement to tell us
    // which index has its value incremented.

    let mut counters: Vec<i32> = vec![0; 26];
    let word_vector: Vec<char> = word.chars().collect();

    let mut two = 0;
    let mut three = 0;

    for letter in word_vector {
        let index = letter_to_index(letter);
        counters[index] += 1;
    }

    if counters.contains(&2) {
        two = 1;
    }

    if counters.contains(&3) {
        three = 1;
    }

    return (two, three);
}

fn letter_to_index(letter: char) -> usize {
    match letter {
        'a' => return 0,
        'b' => return 1,
        'c' => return 2,
        'd' => return 3,
        'e' => return 4,
        'f' => return 5,
        'g' => return 6,
        'h' => return 7,
        'i' => return 8,
        'j' => return 9,
        'k' => return 10,
        'l' => return 11,
        'm' => return 12,
        'n' => return 13,
        'o' => return 14,
        'p' => return 15,
        'q' => return 16,
        'r' => return 17,
        's' => return 18,
        't' => return 19,
        'u' => return 20,
        'v' => return 21,
        'w' => return 22,
        'x' => return 23,
        'y' => return 24,
        'z' => return 25,
        _ => panic!("Non-letter char was passed."),
    }
}

fn part_two(potential_ids: Vec<String>) -> String {
    // For this method, loop over all combinations of two in the list of IDs, without
    // making any redundant comparisons. From part one, there are 250 members to check.

    'compare:
    for i in 0..potential_ids.len() {
        for j in (i+1)..potential_ids.len() {
            let word_one = &potential_ids[i];
            let word_two = &potential_ids[j];

            if compare_words(&word_one, &word_two) {
                let letter_diff = letters_uncommon(&word_one, &word_two);
                return letter_diff;
            }
        }
    }

    return "No answer found!".to_string();
}

fn compare_words(word_one: &String, word_two: &String) -> bool {
    // Compares two words and returns false if they differ by more than one letter.

    let vec_one: Vec<char> = word_one.chars().collect();
    let vec_two: Vec<char> = word_two.chars().collect();

    if vec_one.len() != vec_two.len() {
        panic!("The IDs are not the same length!");
    }

    let mut letter_diff_count = 0;

    for i in 0..vec_one.len() {
        if letter_diff_count > 1 {
            return false;
        }

        if vec_one[i] != vec_two[i] {
            letter_diff_count += 1;
        }
    }

    if letter_diff_count != 1 {
        panic!("IDs should differ by one, but do not! (off by {}", letter_diff_count);
    }

    return true;
}

fn letters_uncommon(word_one: &String, word_two: &String) -> String {
    // This function compares two words and adds those that they have in common to the
    // return value of the function. This way we don't have to compare the two answer
    // IDs by hand.

    let vec_one: Vec<char> = word_one.chars().collect();
    let vec_two: Vec<char> = word_two.chars().collect();

    let mut result: Vec<char> = [].to_vec();

    if vec_one.len() != vec_two.len() {
        panic!("The IDs are not the same length!");
    }

    for i in 0..vec_one.len() {
        if vec_one[i] == vec_two[i] {
            result.push(vec_one[i]);
        }
    }

    return result.into_iter().collect();
}
