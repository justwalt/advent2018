// Problem 1 - https://adventofcode.com/2018/day/1

// Part 1
// The provided text file (input.txt) contains numbers in the form of signed integers.
// The task is to add every number in the text file to a running total, then enter that
// total in the answer box on the website.

// Part 2
// The second part of the problem is looking for the first value of the total that is
// repeated, if the input list is looped over.


// I've converted this simple function into a library so I can use it (and others) for
// future problems.
extern crate adventlib;
use adventlib::return_file_as_int_vector;

fn main() {
    let input_vector = return_file_as_int_vector("input.txt");
    let answer_part_one = part_one(&input_vector);
    let answer_part_two = part_two(&input_vector);

    println!("The answer to part one is [{}].", answer_part_one);
    println!("The answer to part two is [{}].", answer_part_two);
}

fn part_one(vector: &Vec<i64>) -> i64 {
    // Part one can be achieved with a very simple loop.

    let mut total: i64 = 0;
    for value in vector {
        total += value;
    }

    return total;
}

fn part_two(vector: &Vec<i64>) -> i64 {
    // Part two is slightly less simple, and running a non-optimized version will take
    // about 10 minutes. It requires 142 loops, where they begin to take up to 5 seconds
    // per loop.

    let mut total: i64 = 0;
    let mut history: Vec<i64> = [0].to_vec();
    let mut loops: i64 = 0;
    
    loop {
        loops += 1;
        println!("Loop number: {}", loops);

        for value in vector {
            total += value;

            if history.contains(&total) {
                return total;
            }

            history.push(total);
        }
    }
}