total = 0
history = [0]

with open("input.txt", "r") as file:
    while True:
        for line in file:
            total += int(line)

            if total in history:
                print(total)
                break

            history.append(total)
