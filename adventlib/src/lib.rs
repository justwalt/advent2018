#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

use std::fs;
use std::io::{BufRead, BufReader};

pub fn return_file_as_int_vector(filename: &str) -> Vec<i64> {
    let mut input_vector: Vec<i64> = [].to_vec();

    let file_innards = fs::File::open(filename)
        .expect("Couldn't read the input file!");

    for line in BufReader::new(&file_innards).lines() {
        let line_value: i64 = line.unwrap().parse().unwrap();
        input_vector.push(line_value);
    }

    return input_vector;
}

pub fn return_file_as_str_vector(filename: &str) -> Vec<String> {
    let mut input_vector: Vec<String> = [].to_vec();

    let file_innards = fs::File::open(filename)
        .expect("Couldn't read the input file!");

    for line in BufReader::new(&file_innards).lines() {
        let line_value: String = line.unwrap().parse().unwrap();
        input_vector.push(line_value);
    }

    return input_vector;
}
